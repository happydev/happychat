# Infra

## Contribute

```
# docker-compose up -d
```

The Happy Chat is then accessbile at `orbit.localhost` with `admin` login and passord.

And:
 * `rocket.chat` at `rocketchat.localhost` (administrator login is `batman` / `admin`)
 * `djangoldp` server at `djangoldp.localhost`.
