## Test Django OIDC / Rocketchat

### On rocket.chat

Add a custom provider `http://rocketchat.localhost/admin/OAuth`:

* App name: `Django OIDC`
* Service URL: `http://djangoldp.localhost`
* Token: `http://djangoldp:8000/token`
* Sent from: `header`
* Identifaction: `http://djangoldp:8000/userinfo`
* Authorize: `/authorize`
* Scope: `openid profile email`
* Token param: `access_token`
* ID: `rocket_client`
* Secret: `verysecretkey`
* Connection: `Redirect`
* Key field: `user`
* User name: `preferred_username`
* Email: `email`
* Name: `name`

In `http://rocketchat.localhost/admin/Accounts`:

 * disable 2FA
 * disable default login windows

### On DjangoLDP

Add OAuth client:

* Name: `Chat`
* Client type: `confidential`
* Response Type: `Authorization code grant`
* Redirect URI: `http://rocketchat.localhost/_oauth/djangooidc`
* Scope: `openid profile email`

### Test it

Create new user on `djangoLDP` with username and email and try to log with it on rockerchat.localhost.
