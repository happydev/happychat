# Technical specifications

## Summary of requirements

Required features from business:

1. Chat
2. Directory
3. Jobboard
4. Onboarding
5. News feed
6. Documentation
7. File sharing
8. Invoicing

## Architecture proposal

```
                ┌────────────┐
      ┌─────────┤ID PROVIDER ├─────────┐
      │         └─────┬──────┘         │
      │               │                │
┌─────▼──────┐ ┌──────▼───────┐ ┌──────▼──────┐
│ ROCKETCHAT │ │SIB DIRECTORY │ │SIB JOBBOARD │
└────────────┘ └──────────────┘ └─────────────┘
```
