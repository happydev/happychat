*** Settings ***
Library     Browser

*** Test Cases ***

LDP server login
    New Browser     firefox
    New Page        http://djangoldp:8000/admin/login/
    Type Text       input#id_username   admin
    Type Text       input#id_password   admin
    Click           text=Log In
    Get Title       ==  Site administration | Django site admin
    Close Browser
